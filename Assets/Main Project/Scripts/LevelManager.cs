﻿using UnityEngine;

public class LevelManager : MonoBehaviour
{
    #region Fields
    // private
    static LevelManager instance;
    bool pause;
    bool speedChangeIsActive;
    float speedChangedTime;
    #endregion

    #region Properties
    public static LevelManager Instance
    {
        get { return instance; }
    }
    public bool Pause
    {
        get { return pause; }
    }
    public float Speed
    {
        get { return Time.timeScale; }
        set { SetGameSpeed(value); }
    }
    #endregion

    #region Methods
    void Awake()
    {
        Initialize();
    }
    void Initialize()
    {
        instance = this;
        SetGameSpeed(1);
        pause = false;
    }
    void Update()
    {
        if(!Pause)
        {
            if (speedChangeIsActive)
                CountDownTimeChange();
        }
    }
    // Time manipulation
    void SetGameSpeed(float value)
    {
        Time.timeScale = value;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
    }
    public void SetGameSpeedTemporary(float speed, float effectTime)
    {
        if (speed <= 0)
            return;
        speedChangeIsActive = true;
        speedChangedTime = effectTime / (1 / speed);
        SetGameSpeed(speed);
    }
    void CountDownTimeChange()
    {
        speedChangedTime -= Time.deltaTime;
        if(speedChangedTime <= 0)
        {
            SetGameSpeed(1);
            speedChangedTime = 0;
            speedChangeIsActive = false;
        }
    }
    // Pauses
    public void StartPause()
    {
        pause = true;
    }
    public void StopPause()
    {
        pause = false;
    }
    #endregion
}
