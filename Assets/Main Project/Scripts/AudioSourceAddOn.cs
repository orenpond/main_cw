﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceAddOn : MonoBehaviour
{
    #region Fields
    [HideInInspector]
    public AudioSource audioSource;
    float startPitch;
    float startVolume;
    List<AudioSourceAddOn> instantiateAudioSources;
    Coroutine fadeOutCoroutine;
    #endregion

    #region Properties
    public AudioSource myAudioSource
    {
        get
        {
            if (audioSource == null)
                audioSource = GetComponent<AudioSource>();
            return audioSource;
        }
    }
    public float StartPitch { get { return startPitch; } }
    public float StartVolume { get { return startVolume; } }
    #endregion

    #region Methods
    void Awake()
    {
        if (audioSource == null)
            audioSource = GetComponentInChildren<AudioSource>();
        startPitch = myAudioSource.pitch;
        startVolume = myAudioSource.volume;
        instantiateAudioSources = new List<AudioSourceAddOn>();
    }
    // Play Method
    public void FadeOut(float fadeTime)
    {
        if (fadeOutCoroutine != null)
            StopCoroutine(fadeOutCoroutine);
        fadeOutCoroutine = StartCoroutine("FadeOutEnum", fadeTime);
    }
    IEnumerator FadeOutEnum(float fadeTime)
    {
        float timePassed = 0;
        while(timePassed < fadeTime)
        {
            timePassed += Time.deltaTime;
            myAudioSource.volume = Mathf.Lerp(startVolume, 0f, timePassed / fadeTime);
            yield return 0;
        }
    }
    // Instantiate Methods
    void Instantiate(bool playMulti, float pitchChange, bool changePitchByCount, bool randomPitch)
    {
        if (AudioManager.Instance == null)
            return;

        if (instantiateAudioSources == null)
            instantiateAudioSources = new List<AudioSourceAddOn>();

        instantiateAudioSources.Add( 
            AudioManager.Instance.InstantiateAudioSourceAddOn(this, playMulti, pitchChange, changePitchByCount, randomPitch));
    }
    public void InstantiateMulti()
    {
        Instantiate(true, 0 , false, false);
    }
    public void InstantiateMulti(float pitchAddOn)
    {
        Instantiate(true, pitchAddOn, true, false);
    }
    public void InstantiateMultiRandom(float pitchAddOn)
    {
        Instantiate(true, pitchAddOn, false, true);
    }
    public void InstantiateOnce()
    {
        Instantiate(false, 0, false, false);
    }
    public void InstantiateOnce(float pitchAddOn)
    {
        Instantiate(false, pitchAddOn, true, false);
    }
    public void StopAllThisTypeAudioSource()
    {
        if(instantiateAudioSources != null)
        {
            foreach (AudioSourceAddOn audioSourceAddOn in instantiateAudioSources)
            {
                if (audioSourceAddOn != null)
                {
                    if(audioSourceAddOn.audioSource != null)
                        audioSourceAddOn.audioSource.Stop();
                }
            }
        }
    }
    public void StopLastTypeAudioSource()
    {
        if (instantiateAudioSources != null)
        {
            if(instantiateAudioSources.Count > 0)
            {
                int index = instantiateAudioSources.Count - 1;
                if (instantiateAudioSources[index] != null)
                {
                    if (instantiateAudioSources[index].audioSource != null)
                        instantiateAudioSources[index].audioSource.Stop();
                }
            }
        }
    }
    #endregion
}
