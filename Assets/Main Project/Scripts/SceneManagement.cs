﻿using UnityEngine.SceneManagement;

public static class SceneManagement
{
    #region Methods
    public static void ResetScene()
    {
        // Load The Same Scene Back
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public static void GoToNextScene()
    {
        int index = SceneManager.GetActiveScene().buildIndex;
        if (index + 1 < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(index + 1);
        }
        else // return to first level
        {
            SceneManager.LoadScene(0);
        }
    }
    #endregion
}
