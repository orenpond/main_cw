﻿using UnityEngine;

public class MaterialsBank : MonoBehaviour
{
    #region Fields
    // private fields
    [SerializeField] Material blankCardMaterial;
    [SerializeField] Material defaultBackCardMaterial;
    [SerializeField] Material backCardBlackMaterial;
    [SerializeField] Material backCardRedMaterial;
    [SerializeField] Material backCardBlueMaterial;
    [SerializeField] Material clubs02Material;
    [SerializeField] Material clubs03Material;
    [SerializeField] Material clubs04Material;
    [SerializeField] Material clubs05Material;
    [SerializeField] Material clubs06Material;
    [SerializeField] Material clubs07Material;
    [SerializeField] Material clubs08Material;
    [SerializeField] Material clubs09Material;
    [SerializeField] Material clubs10Material;
    [SerializeField] Material clubs11Material;
    [SerializeField] Material clubs12Material;
    [SerializeField] Material clubs13Material;
    [SerializeField] Material clubs14Material;
    [SerializeField] Material diamonds02Material;
    [SerializeField] Material diamonds03Material;
    [SerializeField] Material diamonds04Material;
    [SerializeField] Material diamonds05Material;
    [SerializeField] Material diamonds06Material;
    [SerializeField] Material diamonds07Material;
    [SerializeField] Material diamonds08Material;
    [SerializeField] Material diamonds09Material;
    [SerializeField] Material diamonds10Material;
    [SerializeField] Material diamonds11Material;
    [SerializeField] Material diamonds12Material;
    [SerializeField] Material diamonds13Material;
    [SerializeField] Material diamonds14Material;
    [SerializeField] Material hearts02Material;
    [SerializeField] Material hearts03Material;
    [SerializeField] Material hearts04Material;
    [SerializeField] Material hearts05Material;
    [SerializeField] Material hearts06Material;
    [SerializeField] Material hearts07Material;
    [SerializeField] Material hearts08Material;
    [SerializeField] Material hearts09Material;
    [SerializeField] Material hearts10Material;
    [SerializeField] Material hearts11Material;
    [SerializeField] Material hearts12Material;
    [SerializeField] Material hearts13Material;
    [SerializeField] Material hearts14Material;
    [SerializeField] Material spades02Material;
    [SerializeField] Material spades03Material;
    [SerializeField] Material spades04Material;
    [SerializeField] Material spades05Material;
    [SerializeField] Material spades06Material;
    [SerializeField] Material spades07Material;
    [SerializeField] Material spades08Material;
    [SerializeField] Material spades09Material;
    [SerializeField] Material spades10Material;
    [SerializeField] Material spades11Material;
    [SerializeField] Material spades12Material;
    [SerializeField] Material spades13Material;
    [SerializeField] Material spades14Material;
    static MaterialsBank instance;
    #endregion

    #region Proerties
    public Material BlankCardMaterial { get { return blankCardMaterial; } }
    public Material DefaultBackCardMaterial { get { return defaultBackCardMaterial; } }
    public Material BackCardBlackMaterial { get { return backCardBlackMaterial; } }
    public Material BackCardBlueMaterial { get { return backCardBlueMaterial; } }
    public Material BackCardRedMaterial { get { return backCardRedMaterial; } }
    public static MaterialsBank Instance { get { return instance; } }
    #endregion

    #region Methods
    void Awake()
    {
        Initialize();
    }
    void Initialize()
    {
        if (blankCardMaterial == null)
            Debug.LogError("blankCardMaterial is null on " + gameObject.name);
        if (defaultBackCardMaterial == null)
            Debug.LogError("defaultBackCardMaterial is null on " + gameObject.name);
        instance = this;
    }
    public Material GetClubs(int number)
    {
        Material result = null;

        switch(number)
        {
            case 2: result = clubs02Material; break;
            case 3: result = clubs03Material; break;
            case 4: result = clubs04Material; break;
            case 5: result = clubs05Material; break;
            case 6: result = clubs06Material; break;
            case 7: result = clubs07Material; break;
            case 8: result = clubs08Material; break;
            case 9: result = clubs09Material; break;
            case 10: result = clubs10Material; break;
            case 11: result = clubs11Material; break;
            case 12: result = clubs12Material; break;
            case 13: result = clubs13Material; break;
            case 14: result = clubs14Material; break;
        }
        if (result == null)
            result = blankCardMaterial;
        return result;
    }
    public Material GetDiamonds(int number)
    {
        Material result = null;

        switch (number)
        {
            case 2: result = diamonds02Material; break;
            case 3: result = diamonds03Material; break;
            case 4: result = diamonds04Material; break;
            case 5: result = diamonds05Material; break;
            case 6: result = diamonds06Material; break;
            case 7: result = diamonds07Material; break;
            case 8: result = diamonds08Material; break;
            case 9: result = diamonds09Material; break;
            case 10: result = diamonds10Material; break;
            case 11: result = diamonds11Material; break;
            case 12: result = diamonds12Material; break;
            case 13: result = diamonds13Material; break;
            case 14: result = diamonds14Material; break;
        }
        if (result == null)
            result = blankCardMaterial;
        return result;
    }
    public Material GetHearts(int number)
    {
        Material result = null;

        switch (number)
        {
            case 2: result = hearts02Material; break;
            case 3: result = hearts03Material; break;
            case 4: result = hearts04Material; break;
            case 5: result = hearts05Material; break;
            case 6: result = hearts06Material; break;
            case 7: result = hearts07Material; break;
            case 8: result = hearts08Material; break;
            case 9: result = hearts09Material; break;
            case 10: result = hearts10Material; break;
            case 11: result = hearts11Material; break;
            case 12: result = hearts12Material; break;
            case 13: result = hearts13Material; break;
            case 14: result = hearts14Material; break;
        }
        if (result == null)
            result = blankCardMaterial;
        return result;
    }
    public Material GetSpades(int number)
    {
        Material result = null;

        switch (number)
        {
            case 2: result = spades02Material; break;
            case 3: result = spades03Material; break;
            case 4: result = spades04Material; break;
            case 5: result = spades05Material; break;
            case 6: result = spades06Material; break;
            case 7: result = spades07Material; break;
            case 8: result = spades08Material; break;
            case 9: result = spades09Material; break;
            case 10: result = spades10Material; break;
            case 11: result = spades11Material; break;
            case 12: result = spades12Material; break;
            case 13: result = spades13Material; break;
            case 14: result = spades14Material; break;
        }
        if (result == null)
            result = blankCardMaterial;
        return result;
    }
    #endregion
}
