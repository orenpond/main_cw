﻿using UnityEngine;
using UnityEngine.Events;

public class Card : MonoBehaviour
{
    #region Fields
    // public fields
    [Header("Gameplay Values")]
    [Range(2, 14)] public int cardNumber;
    public CardSymbol cardSymbol;
    [Header("Material Values")]
    public Material backMaterial;
    public Material frontMaterial;
    [Header("Unity Events")]
    public UnityEvent startDrawingEvent;
    public UnityEvent putOnDrawingDeckEvent;
    public UnityEvent backToWinDeckEvent;
    // private fields
    MeshRenderer meshRenderer;
    Transform myTransform;
    #endregion

    #region Properties
    public Transform MyTransform { get { return myTransform; } }
    #endregion

    #region Enums
    public enum CardSymbol { Clubs, Diamonds, Hearts, Spades};
    #endregion

    #region Methods
    void Awake()
    {
        Initialize();
    }
    void Initialize()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        myTransform = transform;
    }
    public void HideOrShow(bool hide)
    {
        if(meshRenderer != null)
        {
            if (hide)
                meshRenderer.enabled = false;
            else
                meshRenderer.enabled = true;
        }
    }
    public void FlipToBack()
    {
        if (meshRenderer != null)
            meshRenderer.material = backMaterial;
    }
    public void FlipToFront()
    {
        if (meshRenderer != null)
            meshRenderer.material = frontMaterial;
    }
    #endregion
}
