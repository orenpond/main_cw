﻿using System.Collections.Generic;
using UnityEngine;

public class GameplayLoop : MonoBehaviour
{
    #region Fields
    // public fields
    [Header("Players Value")]
    [HideInInspector]public List<Player> players;
    // private fields
    static GameplayLoop instance;
    Dictionary<Player, List<Card>> drawCardsDeck;
    GameState currentGameState;
    Player[] startedPlayers;
    List<Player> winningPlayers;
    List<Player> losingPlayers;
    List<Player> tiePlayers;
    List<Player> playersThatEndedTheirRound;
    #endregion

    #region Properties
    public static GameplayLoop Instance { get { return instance; } }
    #endregion

    #region Enums
    public enum GameState { WaitToStart, RegularFight, War, WaitingToEndRound, EndGame};
    #endregion

    #region Methods
    void Awake()
    {
        Initialize();
    }
    void Initialize()
    {
        instance = this;
        drawCardsDeck = new Dictionary<Player, List<Card>>();
        currentGameState = GameState.WaitToStart;
        winningPlayers = new List<Player>();
        losingPlayers = new List<Player>();
        playersThatEndedTheirRound = new List<Player>();
    }
    /// <summary>
    /// Assign the players field values from extranl class BoardInitializer.cs During it's Start Event
    /// </summary>
    public void RegisterPlayers(List<BoardInitializer.PlayerOnBoard> playersOnBoard)
    {
        players = new List<Player>();
        for (int i = 0; i < playersOnBoard.Count; i++)
        {
            if (playersOnBoard[i].isActive && playersOnBoard[i].player != null)
                players.Add(playersOnBoard[i].player);
        }
        startedPlayers = players.ToArray();
    }
    /// <summary>
    /// The KickStart to start the mainbattle, can be turn on externally or internally
    /// Without running it the main battle won't start externally
    /// </summary>
    public void StartMainBattleLoop()
    {
        // show all players deck
        if(players != null)
        {
            foreach(Player player in players)
            {
                if (player != null)
                    player.HideOrShowDeck(false);
            }
        }
        // change game state
        ChangeGameState(GameState.RegularFight);
    }
    void Update()
    {
        if (!LevelManager.Instance.Pause)
        {
            MainBattleLoop();
        }
    }
    /// <summary>
    /// The big game loop run's everything from the first time until reaching the end of the game
    /// (the ending is excluded from this loop)
    /// </summary>
    void MainBattleLoop()
    {
        if (currentGameState == GameState.RegularFight || currentGameState == GameState.War)
        {
            if (players != null)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    if (currentGameState != GameState.EndGame)
                    {
                        if (players[i] != null)
                        {
                            // check if the player before, has finished it's turn
                            if (i > 0)
                            {
                                if (players[i - 1]._PlayStatus == Player.PlayStatus.WaitingFotInput
                                    || players[i - 1]._PlayStatus == Player.PlayStatus.OnDrawing
                                    || players[i - 1]._PlayStatus == Player.PlayStatus.OnRoundEnd
                                    || players[i - 1]._PlayStatus == Player.PlayStatus.EndedRound)
                                    break;
                            }

                            // draw card
                            if (players[i]._PlayStatus == Player.PlayStatus.WaitingFotInput
                                && currentGameState != GameState.WaitingToEndRound)
                            {
                                if (players[i].playedBy == Player.PlayedBy.Human)
                                {
                                    if (PlayerInput())
                                        DrawCard(players[i]);
                                }
                                else
                                    DrawCard(players[i]);
                            }
                        }
                    }
                }
                if (currentGameState != GameState.EndGame)
                    CalculateDrawingResults();
            }
        }
    }
    /// <summary>
    /// Check if human player want to draw a-card from it's deck
    /// </summary>
    bool PlayerInput()
    {
        if (Input.GetMouseButtonDown(0))
            return true;
        return false;
    }
    /// <summary>
    /// The first method to start drawing a card from player deck
    /// </summary>
    void DrawCard(Player player)
    {
        // validate
        if (player == null)
            return;

        if (player._PlayStatus == Player.PlayStatus.OnDrawing)
            return;

        if (drawCardsDeck == null)
            return;

        // put next card on drawCardsDeck
        if(player.myCards != null)
        {
            if (PlayerAbleToDrawCard(player))
            {
                // move card on code 
                int howMuchToDraw = currentGameState == GameState.RegularFight ? 1 : 2;
                int drawCount = 0;
                Card[] drawnCards = new Card[howMuchToDraw];
                do
                {
                    drawnCards[drawCount] = player.myCards[0];
                    if (!drawCardsDeck.ContainsKey(player))
                        drawCardsDeck.Add(player, new List<Card>());
                    drawCardsDeck[player].Add(player.myCards[0]);
                    player.myCards.RemoveAt(0);
                    drawCount++;
                } while (drawCount < howMuchToDraw);
                // continue drawing procedure
                player.DrawCards(drawnCards,
                    currentGameState == GameState.RegularFight ? Player.DrawType.Regular : Player.DrawType.War);
            }
            else // if player have no cards to draw
            {
                if (currentGameState == GameState.RegularFight)
                {
                    RegisterLosingPlayers(player);
                    players.Remove(player);
                    CheckIfGameFinished();
                }
                if(currentGameState == GameState.War)
                {
                    CheckIfGameFinished();
                }
            }
        }
    }
    /// <summary>
    /// Check if player is able to draw a card from it's deck
    /// </summary>
    bool PlayerAbleToDrawCard(Player player)
    {
        // validate
        if (player == null)
            return false;
        if (drawCardsDeck == null)
            return false;

        // put next card on drawCardsDeck
        if (player.myCards != null)
        {
            // check if able to draw
            if (currentGameState == GameState.RegularFight)
            {
                if (player.myCards.Count - 1 >= 0)
                    return true;
            }
            if (currentGameState == GameState.War)
            {
                if (player.myCards.Count - 2 >= 0)
                    return true;
            }
        }
        return false;
    }
    /// <summary>
    /// Calculate the drawing result after all the players draw from there deck
    /// </summary>
    void CalculateDrawingResults()
    {
        if(CheckIfAllPlayersDrawCards())
        {
            if(CheckIfThereIsATie())
            {
                // declare war
                ChangeGameState(GameState.War);
                // change all player play status to waiting for input
                foreach (Player player in players)
                    player.ChangePlayStatus(Player.PlayStatus.WaitingFotInput);
            }
            else
            {
                RoundWinAndLoseProcedure();
            }
        }
    }
    /// <summary>
    /// Validate if all player draw their cards before calculating the result 
    /// </summary>
    bool CheckIfAllPlayersDrawCards()
    {
        if (players != null)
        {
            foreach (Player player in players)
            {
                if (player != null)
                {
                    if (player._PlayStatus == Player.PlayStatus.WaitingFotInput)
                        return false;
                    if (player._PlayStatus == Player.PlayStatus.OnDrawing)
                        return false;
                }
            }
        }
        return true;
    }
    /// <summary>
    /// Check if drawing result is a tie
    /// </summary>
    bool CheckIfThereIsATie()
    {
        int lastCardIndex = drawCardsDeck[players[0]].Count - 1;
        Player testedPlayer = null;
        for (int i = 0; i < players.Count; i++)
        {
            testedPlayer = players[i];
            foreach (Player otherPlayer in players)
            {
                if (testedPlayer != otherPlayer)
                {
                    if (drawCardsDeck[otherPlayer].Count > lastCardIndex
                        && drawCardsDeck[testedPlayer].Count > lastCardIndex)
                    {
                        if (drawCardsDeck[testedPlayer][lastCardIndex].cardNumber ==
                            drawCardsDeck[otherPlayer][lastCardIndex].cardNumber)
                            return true;
                    }
                }
            }
        }
        return false;
    }
    /// <summary>
    /// Check who's the winner player this round
    /// </summary>
    Player CheckWhoWinsThisRound()
    {
        int lastCardIndex = drawCardsDeck[players[0]].Count - 1;
        Player winningPlayer = players[0];
        foreach (Player otherPlayer in players)
        {
            if (winningPlayer != otherPlayer)
            {
                if(drawCardsDeck[otherPlayer].Count > lastCardIndex
                   && drawCardsDeck[winningPlayer].Count > lastCardIndex)
                {
                    if (drawCardsDeck[winningPlayer][lastCardIndex].cardNumber <
                            drawCardsDeck[otherPlayer][lastCardIndex].cardNumber)
                        winningPlayer = otherPlayer;
                }
            }
        }
        return winningPlayer;
    }
    /// <summary>
    /// After calculating the result on CalculateDrawingResults() if their is not a tie
    /// then we run the win and lose procedure
    /// </summary>
    void RoundWinAndLoseProcedure()
    {
        // change game state
        ChangeGameState(GameState.WaitingToEndRound);
        // check who's winning
        Player winningPlayer = CheckWhoWinsThisRound();
        // add losers card to winning player
        foreach (Player player in players)
            winningPlayer.myCards.AddRange(drawCardsDeck[player]);
        // create an array for all drawCardsDeck cards
        List<Card> winCards = new List<Card>();
        foreach (Player player in players)
            winCards.AddRange(drawCardsDeck[player]);
        // raise win event
        winningPlayer.RaiseRoundWinEvent(winCards.ToArray());
        // clear drawcardsDeck
        foreach (Player player in players)
            drawCardsDeck[player].Clear();
        // raise lose event
        foreach (Player player in players)
        {
            if(player != null)
            {
                if (player != winningPlayer)
                    player.RaiseRoundLoseEvent();
            }
        }
    }
    /// <summary>
    /// Check if every player finished it's round.
    /// 
    /// Every player after he finishes it's round run this method externally
    /// from either Player.GetWinningCards enum or Player.LoseReaction()
    /// and if every player reports we start the next round !!!
    /// </summary>
    public void ReportEndOfRound(Player player)
    {
        // validate
        if (player == null)
            return;
        if (playersThatEndedTheirRound == null)
            playersThatEndedTheirRound = new List<Player>();

        // check if player ended is round
        if(player._PlayStatus == Player.PlayStatus.EndedRound)
        {
            if (!playersThatEndedTheirRound.Contains(player))
                playersThatEndedTheirRound.Add(player);

            // check if all player ended their round
            foreach (Player p in players)
            {
                if(p != null)
                {
                    if (p._PlayStatus != Player.PlayStatus.EndedRound)
                        return;
                }
            }

            // all players finished so we noe return to regular fight

            // change all player play status to waiting for input
            foreach (Player p in players)
                p.ChangePlayStatus(Player.PlayStatus.WaitingFotInput);
            // return to regular mode
            ChangeGameState(GameState.RegularFight);
            // clear playersThatEndedTheirRound list
            playersThatEndedTheirRound.Clear();
        }

    }
    void ChangeGameState(GameState newState)
    {
        currentGameState = newState;
    }
    /// <summary>
    /// Register the losing players for end result
    /// </summary>
    void RegisterLosingPlayers(Player loser)
    {
        if (loser == null)
            return;

        if (losingPlayers == null)
            losingPlayers = new List<Player>();

        losingPlayers.Add(loser);
    }
    /// <summary>
    /// Register the winning players for end result
    /// </summary>
    void RegisterWinningPlayer(Player winner)
    {
        if (winner == null)
            return;

        if (winningPlayers == null)
            winningPlayers = new List<Player>();

        winningPlayers.Add(winner);
    }
    /// <summary>
    /// After one of the player losses it's cards we check if the game finished.
    /// If so we run FinishGameProcedure() and exit the main battle loop.
    /// 
    /// we call this method from DrawCard() if the player lost all it's cards.
    /// </summary>
    void CheckIfGameFinished()
    {
        if (currentGameState == GameState.RegularFight)
        {
            if (players.Count == 1)
            {
                if (players[0].myCards.Count > 0)
                {
                    RegisterWinningPlayer(players[0]);
                    FinishGameProcedure();
                }
            }
        }
        if (currentGameState == GameState.War)
        {
            // check whi unable to draw
            List<Player> unableToDraw = new List<Player>();
            for (int i = 0; i < players.Count; i++)
            {
                if (!PlayerAbleToDrawCard(players[i]))
                    unableToDraw.Add(players[i]);
            }
            if (unableToDraw.Count == players.Count)
            {
                // set all left players to a tie result
                tiePlayers = new List<Player>();
                for (int i = 0; i < players.Count; i++)
                    tiePlayers.Add(players[i]);

                // finish the game
                FinishGameProcedure();
            }
            else
            {
                // register all losing players
                for (int i = 0; i < unableToDraw.Count; i++)
                {
                    RegisterLosingPlayers(unableToDraw[i]);
                    players.Remove(unableToDraw[i]);
                }
                // check if theres a winner
                if (players.Count == 1)
                {
                    RegisterWinningPlayer(players[0]);
                    FinishGameProcedure();
                }
            }
        }
    }
    void FinishGameProcedure()
    {
        ChangeGameState(GameState.EndGame);
        Debug.Log("Mazal Tov The Game Is Finished");
        // Need To Do
        // Add Visual Reaction
        // Show Result
        // Go To Main Menu
    }

    /// <summary>
    /// Check how much cards we currently have at this round, from all the players on their drewing deck.
    /// 
    /// (Used by Player.WarDrawEnum as temp solution for calculating the needed height for the card).
    /// </summary>
    public int HowMuchCardsOnTheDrewingDeck(Player player)
    {
        if (player == null)
            return 0;
        if (drawCardsDeck == null)
            return 0;
        if (!drawCardsDeck.ContainsKey(player))
            return 0;
        if (drawCardsDeck[player] == null)
            return 0;

        return drawCardsDeck[player].Count;
    }
    #endregion
}
