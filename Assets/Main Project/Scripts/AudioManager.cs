﻿using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region Fields
    [HideInInspector]
    public Dictionary<AudioClip, List<AudioSourceAddOn>> audioSourceAndClip;
    static AudioManager instance;
    List<AudioSourceAddOn> tempAudioSourceList;
    AudioSourceAddOn foundSourceOnList;
    Dictionary<AudioClip, int> audioSourceAndClipCount;
    #endregion

    #region Properties
    public static AudioManager Instance { get { return instance; } }
    #endregion

    #region Methods
    void Awake()
    {
        instance = this;
        if (audioSourceAndClip == null)
            audioSourceAndClip = new Dictionary<AudioClip, List<AudioSourceAddOn>>();
        if (audioSourceAndClipCount == null)
            audioSourceAndClipCount = new Dictionary<AudioClip, int>();
    }
    public AudioSourceAddOn InstantiateAudioSourceAddOn(AudioSourceAddOn audioSourceAddon, bool playSameClipMultipleTime, float audioPitchAddOn, bool pitchChangeToSourceCount, bool randomPitch)
    {
        if (audioSourceAddon == null)
            return null;

        if (audioSourceAddon.myAudioSource == null)
            return null;

        if (audioSourceAddon.myAudioSource.clip == null)
        {
            return null;
        }
        if (audioSourceAndClip == null)
            audioSourceAndClip = new Dictionary<AudioClip, List<AudioSourceAddOn>>();

        if (!audioSourceAndClip.ContainsKey(audioSourceAddon.myAudioSource.clip))
            audioSourceAndClip[audioSourceAddon.myAudioSource.clip] = new List<AudioSourceAddOn>();

        if (audioSourceAndClip.ContainsKey(audioSourceAddon.myAudioSource.clip))
        {
            foundSourceOnList = null;
            tempAudioSourceList = audioSourceAndClip[audioSourceAddon.myAudioSource.clip];

            for (int i = 0; i < tempAudioSourceList.Count; i++)
            {
                if(tempAudioSourceList[i] != null)
                {
                    if (!tempAudioSourceList[i].myAudioSource.isPlaying)
                    {
                        foundSourceOnList = tempAudioSourceList[i];
                        if (playSameClipMultipleTime)
                            break;
                    }
                    else
                    {
                        if (!playSameClipMultipleTime)
                        {
                            return null;
                        }
                    }
                }
            }
            if(foundSourceOnList == null)
            {
                foundSourceOnList = Instantiate<AudioSourceAddOn>(audioSourceAddon, transform);
                tempAudioSourceList.Add(foundSourceOnList);
            }
        }

        if (!audioSourceAndClipCount.ContainsKey(foundSourceOnList.myAudioSource.clip))
            audioSourceAndClipCount[foundSourceOnList.myAudioSource.clip] = 0;
        audioSourceAndClipCount[foundSourceOnList.myAudioSource.clip]++;
        if (pitchChangeToSourceCount)
            foundSourceOnList.myAudioSource.pitch = foundSourceOnList.StartPitch + (audioPitchAddOn * audioSourceAndClipCount[foundSourceOnList.myAudioSource.clip]);
        else if (randomPitch)
            foundSourceOnList.myAudioSource.pitch = foundSourceOnList.StartPitch + Random.Range(-audioPitchAddOn, audioPitchAddOn);
        else
            foundSourceOnList.myAudioSource.pitch = foundSourceOnList.StartPitch + audioPitchAddOn;
        foundSourceOnList.myAudioSource.volume = foundSourceOnList.StartVolume;
        foundSourceOnList.myAudioSource.Play();
        return foundSourceOnList;
    }
    #endregion
}
