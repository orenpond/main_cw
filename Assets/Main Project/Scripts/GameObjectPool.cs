﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool : MonoBehaviour
{
    #region Fields
    // private
    [SerializeField]
    int poolStartCount = 9;
    static GameObjectPool instance;
    Dictionary<GameObject, List<GameObject>> myGameObjects;
    GameObject tempGameObject;
    Transform myTransform;
    GameObject instantiateGameObject;
    #endregion

    #region Properties
    public static GameObjectPool Instance { get { return instance; } }
    #endregion

    #region Methods
    void Awake()
    {
        Initializer();
    }
    void Initializer()
    {
        instance = this;
        myTransform = transform;
        myGameObjects = new Dictionary<GameObject, List<GameObject>>();
    }
    public GameObject GetGameObject(GameObject gameObjectPrefab)
    {
        // valdiate
        if (gameObjectPrefab == null)
            return null;

        if (myGameObjects == null)
            myGameObjects = new Dictionary<GameObject, List<GameObject>>();

        // check if need to create a pool from scrach
        if (!myGameObjects.ContainsKey(gameObjectPrefab))
        {
            if (CreateGameObjectPool(gameObjectPrefab))
            {
                myGameObjects[gameObjectPrefab][0].SetActive(true);
                return myGameObjects[gameObjectPrefab][0];
            }
        }

        // check if a gameobject is available on the pool
        foreach (GameObject go in myGameObjects[gameObjectPrefab])
        {
            if (go != null)
            {
                if (!go.activeSelf)
                {
                    go.SetActive(true);
                    return go;
                }
            }
        }

        // if there is no one available create a new one
        instantiateGameObject = InstantiateGameObject(gameObjectPrefab);
        instantiateGameObject.SetActive(true);
        return instantiateGameObject;
    }
    public void CreateGameObjectNewPool(GameObject gameObjectPrefab)
    {
        CreateGameObjectPool(gameObjectPrefab);
    }
    bool CreateGameObjectPool(GameObject gameObjectPrefab)
    {
        // validate
        if (gameObjectPrefab == null)
            return false;

        if (myGameObjects == null)
            myGameObjects = new Dictionary<GameObject, List<GameObject>>();

        if (myGameObjects.ContainsKey(gameObjectPrefab))
        {
            if (myGameObjects[gameObjectPrefab] != null)
            {
                if (myGameObjects[gameObjectPrefab].Count > 0)
                    return false;
            }
        }
        // create new pool
        myGameObjects[gameObjectPrefab] = new List<GameObject>();

        for (int i = 0; i < poolStartCount; i++)
            InstantiateGameObject(gameObjectPrefab);

        return true;
    }
    GameObject InstantiateGameObject(GameObject gameObjectPrefab)
    {
        // validate
        if (gameObjectPrefab == null)
            return null;

        if (myGameObjects == null)
            myGameObjects = new Dictionary<GameObject, List<GameObject>>();

        if (!myGameObjects.ContainsKey(gameObjectPrefab))
            myGameObjects[gameObjectPrefab] = new List<GameObject>();

        // create new game object
        tempGameObject = Instantiate<GameObject>(gameObjectPrefab, myTransform.position, myTransform.rotation, myTransform);
        myGameObjects[gameObjectPrefab].Add(tempGameObject);
        tempGameObject.gameObject.SetActive(false);
        return tempGameObject;
    }
    #endregion
}
