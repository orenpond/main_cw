﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    #region Fields
    // public fields
    public PlayedBy playedBy;
    [Header("Postions Values")]
    public Transform drawStartPosition;
    public Transform drawEndPosition;
    public Transform drawReturnPosition;
    [Header("Deck Values")]
    public GameObject cardDeck;
    [Header("Drawing Values")]
    [Range(0,100)]public float drawingTime;
    public AnimationCurve drawingCurve;
    [Header("Getting Cards Values")]
    [Range(0, 100)] public float pauseTimeBeforeGettingCards;
    [Range(0, 100)] public float gettingCardsTime;
    public AnimationCurve gettingCardsCurve;
    [HideInInspector] public List<Card> myCards;
    [Header("Unity Events")]
    public UnityEvent drawRegularCardStart;
    public UnityEvent drawRegularCardFinished;
    public UnityEvent drawWarCardStart;
    public UnityEvent drawWarCardFinished;
    // private fields
    PlayStatus playStatus;
    Coroutine regularDrawCoroutine;
    Coroutine warDrawCoroutine;
    Coroutine getWinningCardsCoroutine;
    Transform myTransform;
    #endregion

    #region Properties
    public PlayStatus _PlayStatus { get { return playStatus; } }
    public int CardCount { get
        {
            if (myCards == null)
                return 0;
            return myCards.Count;
        } }
    #endregion

    #region Enums
    public enum PlayedBy { Human, Computer};
    public enum PlayStatus { WaitingFotInput, OnDrawing, FinishRegularDraw, FinishWarDraw, OnRoundEnd, EndedRound };
    public enum DrawType { Regular, War};
    #endregion

    #region Methods
    void Awake()
    {
        Initialize();
    }
    void Initialize()
    {
        myCards = new List<Card>();
        playStatus = PlayStatus.WaitingFotInput;
        if (drawStartPosition == null)
            Debug.LogError("You need to set drawStartPosition on " + gameObject.name);
        if (drawEndPosition == null)
            Debug.LogError("You need to set drawEndPosition on " + gameObject.name);
        if (drawReturnPosition == null)
            Debug.LogError("You need to set drawReturnPosition on " + gameObject.name);
        if (cardDeck == null)
            Debug.LogError("You need to set cardDeck on " + gameObject.name);
        myTransform = transform;
    }
    /// <summary>
    /// Hide or show player card deck
    /// </summary>
    public void HideOrShowDeck(bool hide)
    {
        if (cardDeck == null)
            return;

        if (hide)
            cardDeck.SetActive(false);
        else
            cardDeck.SetActive(true);
    }
    public void ChangePlayStatus(PlayStatus newStatus)
    {
        playStatus = newStatus;
    }
    /// <summary>
    /// The main (first) method for drawing cards to battle, usuly called from extrnal class: GameplayLoop.cs
    /// </summary>
    public void DrawCards(Card[] drawnCards, DrawType drawType)
    {
        if (drawnCards == null)
            return;

        // hide player deck if there are no cards left
        if (myCards.Count == 0)
            HideOrShowDeck(true);

        // set playStatus to OnDrawingStatus
        ChangePlayStatus(PlayStatus.OnDrawing);

        if (drawType == DrawType.Regular)
        {
            drawRegularCardStart.Invoke();
            regularDrawCoroutine = StartCoroutine("RegularDrawEnum", drawnCards);
        }
        if (drawType == DrawType.War)
        {
            drawWarCardStart.Invoke();
            warDrawCoroutine = StartCoroutine("WarDrawEnum", drawnCards);
        }
    }
    /// <summary>
    /// Enum for drawing card on regular battle
    /// </summary>
    IEnumerator RegularDrawEnum(Card[] drawnCards)
    {
        if (drawnCards != null && drawStartPosition != null && drawEndPosition != null)
        {
            if (drawnCards[0] != null)
            {
                // show card
                drawnCards[0].HideOrShow(false);
                // flip card to front
                drawnCards[0].FlipToFront();
                // raise card event
                drawnCards[0].startDrawingEvent.Invoke();
                // move card to position
                float timePassed = 0;
                while(timePassed < drawingTime)
                {
                    if(!LevelManager.Instance.Pause)
                    {
                        drawnCards[0].MyTransform.position = Vector3.Lerp(drawStartPosition.position,
                            drawEndPosition.position, drawingCurve.Evaluate(timePassed / drawingTime));
                        timePassed += Time.deltaTime;
                    }
                    yield return 0;
                }
                // raise card event
                drawnCards[0].putOnDrawingDeckEvent.Invoke();
                // finish drawing
                FinishRegularDrawing();
            }
        }
    }
    /// <summary>
    /// Enum for drawing cards on war battle
    /// </summary>
    IEnumerator WarDrawEnum(Card[] drawnCards)
    {
        if (drawnCards != null && drawStartPosition != null && drawEndPosition != null)
        {
            if (drawnCards[0] != null && drawnCards[1] != null)
            {
                // calculate card height
                float cardHeight = 0.02f;
                float howMuchCardOnDrawDeck = 0;
                if (GameplayLoop.Instance != null)
                    howMuchCardOnDrawDeck = GameplayLoop.Instance.HowMuchCardsOnTheDrewingDeck(this) - 2;
                // start moving
                float timePassed = 0;
                Vector3 newEndPosition = Vector3.zero;
                for (int cardIndex = 0; cardIndex < 2; cardIndex++)
                {
                    // show card
                    drawnCards[cardIndex].HideOrShow(false);
                    if (cardIndex == 0)
                    {
                        // flip card to back
                        drawnCards[cardIndex].FlipToBack();
                    }
                    else
                    {
                        // flip card to front
                        drawnCards[cardIndex].FlipToFront();
                    }
                    // raise card event
                    drawnCards[cardIndex].startDrawingEvent.Invoke();
                    // move card to position
                    timePassed = 0;
                    newEndPosition = new Vector3(drawEndPosition.position.x, drawEndPosition.position.y,
                        drawEndPosition.position.z - (cardHeight * howMuchCardOnDrawDeck));
                    drawnCards[cardIndex].MyTransform.position = drawStartPosition.position;

                    while (timePassed < drawingTime)
                    {
                        if (!LevelManager.Instance.Pause)
                        {
                            drawnCards[cardIndex].MyTransform.position = Vector3.Lerp(drawStartPosition.position,
                                newEndPosition, drawingCurve.Evaluate(timePassed / drawingTime));
                            timePassed += Time.deltaTime;
                        }
                        yield return 0;
                    }
                    howMuchCardOnDrawDeck++;
                    // raise card event
                    drawnCards[cardIndex].putOnDrawingDeckEvent.Invoke();
                }
                FinishWarDrawing();
            }
        }
    }
    void FinishRegularDrawing()
    {
        drawRegularCardFinished.Invoke();
        ChangePlayStatus(PlayStatus.FinishRegularDraw);
    }
    void FinishWarDrawing()
    {
        drawWarCardFinished.Invoke();
        ChangePlayStatus(PlayStatus.FinishWarDraw);
    }
    /// <summary>
    /// Raised from GameplayLoop.cs to start the round win procedure
    /// </summary>
    public void RaiseRoundWinEvent(Card[] winCards)
    {
        // change staus
        ChangePlayStatus(PlayStatus.OnRoundEnd);
        // show player deck if it was hidden
        if (!cardDeck.activeSelf)
            HideOrShowDeck(false);
        // start getting cards coroutine
        getWinningCardsCoroutine = StartCoroutine("GetWinningCards", winCards);
    }
    /// <summary>
    /// Enum for bringing all the cards from all players draw decks to this player
    /// </summary>
    IEnumerator GetWinningCards(Card[] winCards)
    {
        if (winCards != null && drawReturnPosition != null)
        {
            float timePassed = 0;
            // wait before getting cards
            while (timePassed < pauseTimeBeforeGettingCards)
            {
                if (!LevelManager.Instance.Pause)
                    timePassed += Time.deltaTime;
                yield return 0;
            }
            // get cards
            Vector3 cardStartPos = Vector3.zero;
            for (int i = winCards.Length - 1; i >= 0; i--)
            {
                if (winCards[i] != null)
                {
                    timePassed = 0;
                    cardStartPos = winCards[i].MyTransform.position;
                    while (timePassed < gettingCardsTime)
                    {
                        if (!LevelManager.Instance.Pause)
                        {
                            winCards[i].MyTransform.position = Vector3.Lerp(cardStartPos,
                                myTransform.position, gettingCardsCurve.Evaluate(timePassed / gettingCardsTime));
                            timePassed += Time.deltaTime;
                        }
                        yield return 0;
                    }
                }
                winCards[i].backToWinDeckEvent.Invoke();
                winCards[i].HideOrShow(true);
            }
            // change Status
            ChangePlayStatus(PlayStatus.EndedRound);
            // report to GameplayLoop the round had ended
            if (GameplayLoop.Instance != null)
                GameplayLoop.Instance.ReportEndOfRound(this);
        }
    }
    /// <summary>
    /// Raised from GameplayLoop.cs to start the round lose procedure
    /// </summary>
    public void RaiseRoundLoseEvent()
    {
        ChangePlayStatus(PlayStatus.OnRoundEnd);
        LoseReaction();
    }
    void LoseReaction()
    {
        // report that the round had ended
        ChangePlayStatus(PlayStatus.EndedRound);
        if (GameplayLoop.Instance != null)
            GameplayLoop.Instance.ReportEndOfRound(this);
    }
    #endregion
}
