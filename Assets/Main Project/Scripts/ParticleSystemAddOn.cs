﻿using UnityEngine;
using UnityEngine.Events;

public class ParticleSystemAddOn : MonoBehaviour
{
    #region Fields
    [Header("Instantiate Settings")]
    public bool changePosition;
    public bool changeRotation;
    public bool changeScale;
    [Header("Unity Events")]
    public UnityEvent turnOnEvent;
    public UnityEvent turnOffEvent;
    // private
    ParticleSystem _particleSystem;
    bool startedPausing;
    GameObject instantiateGameObject;
    #endregion

    #region Methods
    public void InstantiateToPool(Transform whereToPut)
    {
        if (GameObjectPool.Instance == null)
            return;

        instantiateGameObject = GameObjectPool.Instance.GetGameObject(gameObject);
        if(changePosition)
            instantiateGameObject.transform.position = whereToPut.position;
        if (changeRotation)
            instantiateGameObject.transform.rotation = whereToPut.rotation;
        if (changeScale)
            instantiateGameObject.transform.localScale = whereToPut.localScale;
    }
    public void Start()
    {

        _particleSystem = GetComponent<ParticleSystem>();
        startedPausing = false;
    }
    public void Update()
    {
        if (!LevelManager.Instance.Pause)
        {
            if (startedPausing)
            {
                _particleSystem.Play();
                startedPausing = false;
            }

            if (_particleSystem.isPlaying)
                return;

            if (_particleSystem.isPaused)
                return;

            gameObject.SetActive(false);
        }
        else
        {
            if (!startedPausing)
            {
                _particleSystem.Pause();
                startedPausing = true;
            }
        }
    }
    void OnEnable()
    {
        if (_particleSystem != null)
            _particleSystem.Play();
        turnOnEvent.Invoke();
    }
    void OnDisable()
    {
        if (_particleSystem != null)
            _particleSystem.Stop();
        turnOffEvent.Invoke();
    }
    #endregion
}
