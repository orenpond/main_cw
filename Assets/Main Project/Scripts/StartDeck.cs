﻿using UnityEngine;

public class StartDeck : MonoBehaviour
{
    #region Methods
    /// <summary>
    /// Start The MainBattleLoop on GameplayLoop.cs after distribute animation ended
    /// In this case using Animation Event on it's distribute animation
    /// </summary>
    public void FinishedDistributeAnimation()
    {
        if (GameplayLoop.Instance != null)
            GameplayLoop.Instance.StartMainBattleLoop();
    }
    #endregion
}
