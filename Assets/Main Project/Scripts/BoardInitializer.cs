﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardInitializer : MonoBehaviour
{
    #region Fields
    // private Serialize Fields
    [Header("Prefabs")]
    [SerializeField] Card cardPrefab;

    [Header("Shuffle Values")]
    [Tooltip("How many times you want to shuffle all the cards")]
    [Range(1, 100)] [SerializeField] uint shuffleLoopCount = 1;

    [Header("Gameplay Values")]
    [SerializeField] List<PlayerOnBoard> playersOnBoard;
    [SerializeField] uint totalCardForPlayer = 26;

    // private
    Card[] allCards;
    uint totalCardCount = 52;
    uint playerCount;
    #endregion

    #region Methods
    void Start()
    {
        Initialize();
    }
    void Initialize()
    {
        if(GameplayLoop.Instance == null)
            Debug.LogError("GameplayLoop.Instance");

        if (cardPrefab == null)
            Debug.LogError(string.Format("cardPrefab is null on {0}", gameObject.name));
        else
        {
            playerCount = 0;
            foreach (PlayerOnBoard playerOnBoard in playersOnBoard)
            {
                if (playerOnBoard.isActive && playerOnBoard.player != null)
                    playerCount++;
            }
            if (playerCount > 1)
            {
                InitializeCards();
            }
            else
                Debug.LogError(string.Format("you need atleast 2 players on {0}", gameObject.name));
        }
    }
    /// <summary>
    /// Run the entire aspect (methods) of creating all the cards for this game.
    /// </summary>
    void InitializeCards()
    {
        CreateNewCards();
        ShuffleCards();
        DistributeCards();
    }
    /// <summary>
    /// Instantiate the cards, give them values and materials and hides them.
    /// </summary>
    void CreateNewCards()
    {
        // validate
        if (cardPrefab == null)
            return;

        // create new cards
        totalCardCount = playerCount * totalCardForPlayer;
        allCards = new Card[totalCardCount];
        int cardNumber = 1;
        int symbolCount = Enum.GetNames(typeof(Card.CardSymbol)).Length;
        for (int i = 0; i < allCards.Length; i += symbolCount)
        {
            cardNumber++;
            for (int d = 0; d < 4; d++)
            {
                // instantiate card
                allCards[i + d] = Instantiate(cardPrefab, this.transform);
                // assign symbol
                switch (d)
                {
                    case 0: allCards[i + d].cardSymbol = Card.CardSymbol.Clubs; break;
                    case 1: allCards[i + d].cardSymbol = Card.CardSymbol.Diamonds; break;
                    case 2: allCards[i + d].cardSymbol = Card.CardSymbol.Hearts; break;
                    case 3: allCards[i + d].cardSymbol = Card.CardSymbol.Spades; break;
                }
                // assign number
                allCards[i + d].cardNumber = cardNumber;
            }
        }
        // Assign Cards Materials
        AssignCardsMaterials();
        // Hide All Cards
        HideAllCards();
    }
    void AssignCardsMaterials()
    {
        if(MaterialsBank.Instance == null)
        {
            Debug.LogError("You need to add MaterialsBank to your scene");
            return;
        }

        if(allCards != null)
        {
            foreach(Card card in allCards)
            {
                if(card != null)
                {
                    card.backMaterial = MaterialsBank.Instance.BackCardRedMaterial;
                    if (card.backMaterial == null)
                        card.backMaterial = MaterialsBank.Instance.DefaultBackCardMaterial;

                    if (card.cardSymbol == Card.CardSymbol.Clubs)
                        card.frontMaterial =  MaterialsBank.Instance.GetClubs(card.cardNumber);
                    if (card.cardSymbol == Card.CardSymbol.Diamonds)
                        card.frontMaterial = MaterialsBank.Instance.GetDiamonds(card.cardNumber);
                    if (card.cardSymbol == Card.CardSymbol.Hearts)
                        card.frontMaterial = MaterialsBank.Instance.GetHearts(card.cardNumber);
                    if (card.cardSymbol == Card.CardSymbol.Spades)
                        card.frontMaterial = MaterialsBank.Instance.GetSpades(card.cardNumber);
                }
            }
        }
    }
    void HideAllCards()
    {
        if(allCards != null)
        {
            foreach(Card card in allCards)
            {
                if (card != null)
                    card.HideOrShow(true);
            }
        }
    }
    void ShuffleCards()
    {
        // validate
        if (allCards == null)
            return;

        // set variables
        int changedIndex = 0;
        Card extractedCard = null;

        // shuffle
        for (int i = 0; i < shuffleLoopCount; i++)
        {
            for (int d = 0; d < allCards.Length; d++)
            {
                changedIndex = Mathf.Clamp(UnityEngine.Random.Range(-1, allCards.Length), 0, allCards.Length - 1);
                extractedCard = allCards[changedIndex];
                allCards[changedIndex] = allCards[d];
                allCards[d] = extractedCard;
            }
        }
    }
    /// <summary>
    /// Distribute Cards to players and also register the players to GameplayLoop
    /// </summary>
    void DistributeCards()
    {
        // validate
        if (allCards == null)
            return;
        if (GameplayLoop.Instance == null)
            return;

        // create new player lists
        GameplayLoop.Instance.RegisterPlayers(playersOnBoard);

        // assign cards to players
        int startIndex = 0, endIndex = (int)totalCardForPlayer;
        for (int i = 0; i < playerCount; i++)
        {
            if (GameplayLoop.Instance.players[i] != null)
            {
                if (GameplayLoop.Instance.players[i].myCards == null)
                    GameplayLoop.Instance.players[i].myCards = new List<Card>();

                for (int d = startIndex; d < endIndex; d++)
                    GameplayLoop.Instance.players[i].myCards.Add(allCards[d]);

                startIndex = endIndex;
                endIndex += (int)totalCardForPlayer;
            }
        }
    }
    #endregion

    #region InnerClasses
    /// <summary>
    /// Used as a field on BoardInitializer to assign multiple players onboard.
    /// </summary>
    [Serializable]
    public class PlayerOnBoard
    {
        public Player player;
        public bool isActive;
    }
    #endregion
}
