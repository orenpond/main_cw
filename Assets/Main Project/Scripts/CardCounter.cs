﻿using UnityEngine;
using TMPro;

/* CardCounter.cs
 * Used to update the card counter text
 */
public class CardCounter : MonoBehaviour
{
    #region Fields
    [SerializeField] Player myPlayer;
    TextMeshPro textMeshPro;
    #endregion

    #region Methods
    void Awake()
    {
        Initialize();
    }
    void Initialize()
    {
        textMeshPro = GetComponentInChildren<TextMeshPro>();
        if (myPlayer == null)
            Debug.LogError("You need to set myPlayer on " + gameObject.name);
    }
    void LateUpdate()
    {
        if(!LevelManager.Instance.Pause)
        {
            UpdateText();
        }
    }
    void UpdateText()
    {
        if (textMeshPro != null && myPlayer != null)
            textMeshPro.text = string.Format("{0}", myPlayer.CardCount);
    }
    #endregion
}
